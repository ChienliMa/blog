class Comment < ActiveRecord::Base
    # relation 
    belongs_to :post

    # data validation
    validates_presence_of :post_id
    validates_presence_of :body
end
