class Post < ActiveRecord::Base
    # validation
    validates_presence_of :title
    validates_presence_of :body

    # relation
    has_many :comments, dependent: :destroy
end
